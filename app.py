from flask import Flask, url_for
from flask_socketio import SocketIO

from flask_session import Session
from src.CIQ import CIQ
from src.network import get_local_ip_address
from src.views import init_app

app = Flask(__name__)

app.config['SECRET_KEY'] = 'tTwoNqJHBTktFsSu'  # Replace this with a random string
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
socketio = SocketIO(app)


@app.before_first_request
def on_startup():
    print(f"Register as an Admin here: {url_for('new_admin', _external=True)}")


if __name__ == '__main__':
    system = CIQ()
    system.shuffle_quiz()

    init_app(app, system, socketio)
    ip = get_local_ip_address()
    socketio.run(app, host=ip, port=5000, debug=True, allow_unsafe_werkzeug=True)
