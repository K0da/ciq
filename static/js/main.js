const canvas = document.getElementById('game-canvas');
const ctx = canvas.getContext('2d');
const quizOverlay = document.getElementById('quiz-overlay');
const qrCodeOverlay = document.getElementById('qr-overlay');
const socket = io.connect(location.origin);

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let characters = [];

const backgroundImages = [
    'static/img/classroom_background.png',
    'static/img/labor_background.png',
    'static/img/library_background.png',
    'static/img/natural_background.png'
].map(src => {
    const img = new Image();
    img.src = src;
    return img;
});

const spriteSheets = [
    {src: 'static/img/Pink_Monster_Idle_4.png', frames: 4},
    {src: 'static/img/Pink_Monster_Walk_6.png', frames: 6},
    {src: 'static/img/Pink_Monster_Attack1_4.png', frames: 4},
    {src: 'static/img/Pink_Monster_Jump_8.png', frames: 8},
    {src: 'static/img/Pink_Monster_Walk+Attack_6.png', frames: 6},
].map(sheet => {
    const img = new Image();
    img.src = sheet.src;
    return {img: img, frames: sheet.frames};
});

function toggleQuizOverlay() {
    quizOverlay.style.display = quizOverlay.style.display === 'none' ? 'flex' : 'none';
}

function toggleQROverlay() {
    qrCodeOverlay.style.display = qrCodeOverlay.style.display === 'none' ? 'flex' : 'none';
}

function createCharacter(name) {
    const character = {
        x: 0,
        y: 0,
        width: 50,
        height: 50,
        currentAnimation: 0,
        frame: 0,
        timer: 0,
        name: name,
        background: backgroundImages[characters.length % backgroundImages.length]
    };
    characters.push(character);
    arrangeCharacters(characters.length);
}

function drawCharacters() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    const numPlayers = characters.length;
    const columns = numPlayers > 9 ? (numPlayers > 12 ? 4 : 3) : 3;
    const rows = Math.ceil(numPlayers / columns);
    const cellWidth = canvas.width / columns;
    const cellHeight = canvas.height / rows;


    // Draw characters with animations
    for (const character of characters) {
        const spriteSheet = spriteSheets[character.currentAnimation]; // Change animation to currentAnimation
        const frameWidth = spriteSheet.img.width / spriteSheet.frames;
        const frameHeight = spriteSheet.img.height;

        // Draw the character's background within their grid cell
        ctx.drawImage(character.background, character.x - (cellWidth - character.width) / 2, character.y - (cellHeight - character.height) / 2, cellWidth, cellHeight);
        ctx.drawImage(
            spriteSheet.img,
            character.frame * frameWidth,
            0,
            frameWidth,
            frameHeight,
            character.x,
            character.y,
            character.width,
            character.height
        );
    }
    drawGridBorders();
}

function drawGridBorders() {
    const numPlayers = characters.length;
    const columns = numPlayers > 9 ? (numPlayers > 12 ? 4 : 3) : 3;
    const rows = Math.ceil(numPlayers / columns);
    const cellWidth = canvas.width / columns;
    const cellHeight = canvas.height / rows;

    ctx.strokeStyle = 'black';
    ctx.lineWidth = 2;

    for (let i = 1; i < rows; i++) {
        ctx.beginPath();
        ctx.moveTo(0, i * cellHeight);
        ctx.lineTo(canvas.width, i * cellHeight);
        ctx.stroke();
    }

    for (let i = 1; i < columns; i++) {
        ctx.beginPath();
        ctx.moveTo(i * cellWidth, 0);
        ctx.lineTo(i * cellWidth, canvas.height);
        ctx.stroke();
    }
}


function updateAnimations() {
    for (const character of characters) {
        character.timer++; // Increment the timer
        if (character.timer > 2.5 * 60) { // 2.5 seconds * 60 (since 60fps is the assumed frame rate)
            character.timer = 0;
            character.currentAnimation = 0; // Set animation back to the default state (idle)
        }
        character.frame = (character.frame + 1) % spriteSheets[character.currentAnimation].frames;
    }
}


function arrangeCharacters(numPlayers) {
    const columns = numPlayers > 9 ? (numPlayers > 12 ? 4 : 3) : 3;
    const rows = Math.ceil(numPlayers / columns);
    const cellWidth = canvas.width / columns;
    const cellHeight = canvas.height / rows;

    for (let i = 0; i < numPlayers; i++) {
        const character = characters[i];
        const row = Math.floor(i / columns);
        const column = i % columns;
        character.x = column * cellWidth + (cellWidth - character.width) / 2;
        character.y = row * cellHeight + (cellHeight - character.height) / 2;
    }
}

function getSelectedCharacter() {
    const characterNumber = document.getElementById('character-number').value;
    if (characterNumber === '' || isNaN(characterNumber)) {
        return null;
    }
    const characterIndex = parseInt(characterNumber, 10) - 1;
    if (characterIndex < 0 || characterIndex >= characters.length) {
        return null;
    }
    return characters[characterIndex];
}

document.getElementById('toggle-quiz').addEventListener('click', toggleQuizOverlay);
document.getElementById('toggle-qr').addEventListener('click', toggleQROverlay);
document.addEventListener('keydown', (event) => {
    if (event.key === '0') {
        createCharacter('Player ' + (characters.length + 1));
        drawCharacters();
    } else if (event.key >= '1' && event.key <= '4') {
        const btnIndex = parseInt(event.key) - 1;
        const selectedCharacter = getSelectedCharacter();
        if (selectedCharacter) {
            selectedCharacter.currentAnimation = btnIndex;
        }
    }
});

socket.on('connect', () => {
    console.log('Connected to the server');
});

socket.on('update_total_names_display', (names) => {
    console.log("Called the update!")
    reload_characters(names)
});

function reload_characters(names) {
    characters = []
    for (const player_name in names) {
        console.log(player_name)
        createCharacter(player_name)
    }
}

// managing Q/A updates
socket.on('update_question_and_answers', handleUpdateQuestionAndAnswers);

function handleUpdateQuestionAndAnswers(data) {
    const question = data.question;
    const answers = data.answers;

    // Update the question text
    const questionElement = document.querySelector('.question-box p');
    questionElement.textContent = question;

    // Update the answers text
    const answerElements = document.querySelectorAll('.answer-box');
    for (let i = 0; i < answers.length; i++) {
        answerElements[i].textContent = `${String.fromCharCode(97 + i)}. ${answers[i]}`;
    }
}

// Add this code to your existing JavaScript inside the event listener for 'DOMContentLoaded'
socket.on('update_question_and_answers', handleUpdateQuestionAndAnswers);


// Main game loop or periodic update function
function gameLoop() {
    updateAnimations();
    drawCharacters();
    requestAnimationFrame(gameLoop);
}

// Start the game loop
gameLoop();
window.onload = function () {
    reload_characters(loaded_names);
}
