const buttonA = document.getElementById('button-a');
const buttonB = document.getElementById('button-b');
const buttonC = document.getElementById('button-c');
const buttonD = document.getElementById('button-d');
const dance = document.getElementById('dance');

const buttons = [buttonA, buttonB, buttonC, buttonD]

const socket = io.connect('http://' + document.domain + ':' + location.port);
socket.on('connect', () => {
    console.log('Connected to server');
});


buttonA.addEventListener('click', () => {
    socket.emit('button_clicked', 'A');
});

buttonB.addEventListener('click', () => {
    socket.emit('button_clicked', 'B');
});

buttonC.addEventListener('click', () => {
    socket.emit('button_clicked', 'C');
});

buttonD.addEventListener('click', () => {
    socket.emit('button_clicked', 'D');
});

dance.addEventListener('click', () => {
    socket.emit('button_clicked', 'dance');
});


socket.on('confirm_button_press', function(buttonValue) {
    button_response_action(buttonValue)

    console.log('Value ' + buttonValue + ' was stored.');
    // Do something with the button value, like updating the UI or sending it to the server
});

socket.on('reset_buttons_color', function () {
    resetButtons()

    console.log('New Question Started')
});

function resetButtons() {
    for (const button of buttons) {
        button.style.backgroundColor = "#58b6a7";
    }
}

function button_response_action(button_value) {
    resetButtons();
    switch (button_value) {
        case 'A': buttonA.style.backgroundColor = "#7F4088FF"; break;
        case 'B': buttonB.style.backgroundColor = "#7F4088FF"; break;
        case 'C': buttonC.style.backgroundColor = "#7F4088FF"; break;
        case 'D': buttonD.style.backgroundColor = "#7F4088FF"; break;
    }
}
