document.addEventListener('DOMContentLoaded', () => {
    const socket = io.connect('http://' + document.domain + ':' + location.port);
    socket.on('connect', () => {
        console.log('Connected to server');
    });

    const button = document.getElementById('next_question');
    button.addEventListener('click', () => {
        socket.emit('next_question');
    });
});