document.addEventListener('DOMContentLoaded', drawChart);

function drawChart() {

  const chartData = {
    labels: [],
    datasets: [{
      data: [],
      backgroundColor: 'rgb(48,90,234)',
      borderColor: 'rgb(0,0,0)',
      borderWidth: 1
    }]
  };

  for (const [category, results] of Object.entries(loaded_result)) {
    chartData.labels.push(category);
    chartData.datasets[0].data.push(results);
  }

  const options = {
    plugins: {
      legend: {
        display: false
      },
      tooltip: {
        enabled: false
      }
    },
    scales: {
      x: {
        ticks: {
          color: '#fff',
          font: {
            size: 35,
            family: 'VT323'
          }
        }
      },
      y: {
        ticks: {
          color: '#fff',
          font: {
            size: 40,
            family: 'VT323'
          },
          stepSize: 1 // Added stepSize property to display full numbers
        }
      }
    }
  };

  const ctx = document.getElementById('resultsChart').getContext('2d');
  const chart = new Chart(ctx, {
    type: 'bar',
    data: chartData,
    options: options
  });
}
