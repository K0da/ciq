const labels = Object.keys(currentUserResult);
const resultData = Object.values(currentUserResult);

const ctx = document.getElementById("barChart").getContext("2d");

const data = {
  labels: labels,
  datasets: [
    {
      data: resultData,
      backgroundColor: "#0E4222",
    },
  ],
};

const options = {
  responsive: true,
  maintainAspectRatio: false,
  indexAxis: "x",
  scales: {
    y: {
      ticks: {
        color: "#fff",
        font: {
          family: "VT323",
          size: 18,
        },
        stepSize: 1 // Added stepSize property to display full numbers
      },
    },
    x: {
      ticks: {
        color: "#fff",
        font: {
          family: "VT323",
          size: 18,
        },
      },
    },
  },
  plugins: {
    legend: {
      display: false,
    },
  },
};

const barChart = new Chart(ctx, {
  type: "bar",
  data: data,
  options: options,
});
