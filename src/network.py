import socket


def get_local_ip_address():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.connect(("8.8.8.8", 80))
            local_ip_address = s.getsockname()[0]
            return local_ip_address
    except Exception as e:
        print(f"Error getting local IP address: {e}")
        return None


if __name__ == "__main__":
    print(get_local_ip_address())
