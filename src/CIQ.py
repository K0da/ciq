import secrets
import string

from src.User import User
from src.questionsDB import Quiz


class CIQ:
    def __init__(self):
        self.users = []
        self.buttons_disabled = False
        self.question_index = 0
        self.quiz_ended = False
        self.admin_code = None
        self.generate_admin_code()
        self.creating_new_admins_allowed = True
        self.quiz = Quiz()

    def shuffle_quiz(self):
        self.quiz.shuffle_quiz()

    def generate_admin_code(self):
        symbols = string.digits
        self.admin_code = ''.join(secrets.choice(symbols) for i in range(4))

    def add_new_user(self, user_id, name):
        if user_id not in [user.id for user in self.users]:
            user = User(user_id, name)
            self.users.append(user)

    def advance_to_next_question(self):
        if len(self.quiz.shuffled_quiz) - 1 > self.question_index:
            self.question_index += 1
        else:
            self.quiz_ended = True
            self.buttons_disabled = True

    def get_current_question(self) -> str:
        if self.quiz_ended:
            return 'Quiz has ended'
        else:
            next_question = self.quiz.shuffled_quiz[self.question_index]['question']
            return next_question

    def get_current_answers(self) -> list:
        if self.quiz_ended:
            return ['', '', '', '']
        else:
            answers = self.quiz.shuffled_quiz[self.question_index]['answers']
            return answers

    def get_result_dict(self) -> dict:
        result_tag_votes = {}

        for question in self.quiz.shuffled_quiz:
            for tag in question["result_tag"]:
                result_tag_votes[tag] = 0

        for user in self.users:
            for idx, vote in enumerate(user.votes):
                if vote is None:
                    continue

                result_tag = self.quiz.shuffled_quiz[idx]["result_tag"][ord(vote) - ord('A')]

                if result_tag not in result_tag_votes:
                    result_tag_votes[result_tag] = 1
                else:
                    result_tag_votes[result_tag] += 1
        return result_tag_votes

    def get_current_user_result_dict(self, current_user) -> dict:
        current_user_result_tag_votes = {}

        for question in self.quiz.shuffled_quiz:
            for tag in question["result_tag"]:
                current_user_result_tag_votes[tag] = 0

        for idx, vote in enumerate(current_user.votes):
            if vote is None:
                continue

            result_tag = self.quiz.shuffled_quiz[idx]["result_tag"][ord(vote) - ord('A')]

            if result_tag not in current_user_result_tag_votes:
                current_user_result_tag_votes[result_tag] = 1
            else:
                current_user_result_tag_votes[result_tag] += 1

        return current_user_result_tag_votes
