import random


class Quiz:
    def __init__(self):
        self.shuffled_quiz = Quiz.quiz.copy()

    def shuffle_quiz(self):
        shuffled_quiz = Quiz.quiz.copy()
        random.shuffle(shuffled_quiz)

        for q in shuffled_quiz:
            answers_and_tags = list(zip(q["answers"], q["result_tag"]))
            random.shuffle(answers_and_tags)
            q["answers"], q["result_tag"] = zip(*answers_and_tags)

        self.shuffled_quiz = shuffled_quiz

    quiz = [
        {
            "question": "Wie gehst du an Problemlösungen heran?",
            "answers": [
                "Indem ich überlege, wie verschiedene Elemente auf einzigartige Weise kombiniert werden können",
                "Durch logische und strukturierte Methoden",
                "Durch Erforschung innovativer Lösungen mit verschiedenen Tools",
                "Indem ich nach neuen Möglichkeiten und Geschäftsideen suche"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Unternehmer'],
        },
        {
            "question": "Welche dieser Aktivitäten interessiert dich am meisten?",
            "answers": ["Kunst, Geschichten oder eindringliche Erlebnisse zu erschaffen",
                        "Werkzeuge und Anwendungen entwickeln, die das Leben erleichtern",
                        "Erlebnisse und Entertainment für andere Menschen zu produzieren",
                        "Neue Geschäftsmöglichkeiten identifizieren und Unternehmen gründen"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Unternehmer'],
        },
        {
            "question": "Bei der Arbeit an einem Projekt, welche dieser Rollen bevorzugst du?",
            "answers": ["Ideen entwickeln und brainstormen",
                        "Daten analysieren und informierte Entscheidungen treffen",
                        "Ideen durch Produktion in die Realität umsetzen",
                        "Die Koordination und Überwachung des Projektfortschritts"],
            "result_tag": ['Kreativprofi', 'Forscher/Analyst', 'Medienproduktionsspezialist', 'Unternehmer'],
        },
        {
            "question": "Wenn du dein eigenes Unternehmen gründen würdest, welche Branche würdest du wählen?",
            "answers": ["Den kreativen Bereich",
                        "Softwareentwicklung",
                        "Marketing, Film, Fernsehen oder Online-Medien",
                        "Jede Branche, in der du ein erfolgreiches Unternehmen aufbauen kannst"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Unternehmer'],
        },
        {
            "question": "Was glaubst du motiviert dich am meisten bei deiner Arbeit?",
            "answers": ["Recherchearbeit, um dadurch komplizierte Sachverhalte lösen zu können",
                        "Ständig dazuzulernen und deine Fähigkeiten in einem Bereich zu verbessern",
                        "Dich kreativ auszudrücken und etwas zu bewirken",
                        "Ein Team zu führen und zu inspirieren, um großartige Ergebnisse zu erzielen"],
            "result_tag": ['Forscher/Analyst', 'Softwareentwickler', 'Kreativprofi', 'Unternehmer'],
        },
        {
            "question": "Wie würdest du am liebsten an einem Projekt arbeiten?",
            "answers": ["Alleine, um meine kreative Vision umzusetzen",
                        "In enger Zusammenarbeit mit einem Team von Entwicklern",
                        "Mit einem vielfältigen Team aus Kreativen und Technikern",
                        "Als Leiter, der die Geschäftsstrategie bestimmt"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Unternehmer'],
        },
        {
            "question": "Was ist dir bei der Arbeit am wichtigsten?",
            "answers": ["Ästhetik und künstlerischer Ausdruck",
                        "Technische Exzellenz und Effizienz",
                        "Die Verbindung von Inhalt und Technologie",
                        "Abstrakte Sachverhalte zu verstehen und selber zu forschen"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Forscher/Analyst'],
        },
        {
            "question": "Welche Art von Umgebung bevorzugst du zum Arbeiten?",
            "answers": ["Ein inspirierender Raum, der meine Kreativität fördert",
                        "Ein ruhiger Ort, an dem ich mich auf technische Aufgaben konzentrieren kann",
                        "Ein dynamisches Studio, in dem verschiedene Projekte gleichzeitig laufen",
                        "Einen strukturierten Arbeitsplatz, an dem Konzentration und Effizienz im Vordergrund stehen"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Forscher/Analyst'],
        },
        {
            "question": "Was ist deine größte Stärke?",
            "answers": ["Kreatives Denken und Ideenfindung",
                        "Problemlösung und analytisches Denken",
                        "Kommunikation und Präsentation von Ideen",
                        "Führung und unternehmerisches Denken"],
            "result_tag": ['Kreativprofi', 'Forscher/Analyst', 'Medienproduktionsspezialist', 'Unternehmer'],
        },
        {
            "question": "Wie verbringst du am liebsten deine Freizeit?",
            "answers": ["Mit kreativen Hobbys wie Malen, Schreiben oder Musik",
                        "Mit technischen Projekten oder Programmierung",
                        "Bei der Erstellung von Videos, Podcasts oder anderen Medien",
                        "Bei der Planung und Umsetzung neuer Geschäftsideen"],
            "result_tag": ['Kreativprofi', 'Softwareentwickler', 'Medienproduktionsspezialist', 'Unternehmer'],
        }
    ]


if __name__ == '__main__':
    q = Quiz()
    q.shuffle_quiz()
    print(q.shuffled_quiz)
