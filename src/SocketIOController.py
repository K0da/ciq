from src.User import User


class SocketIOController:
    def __init__(self, ciq, socketio, session):
        self.ciq = ciq
        self.socketio = socketio
        self.session = session

    def request_next_question(self):
        self.ciq.advance_to_next_question()
        print('Next Question Triggered')
        current_QA: dict = {
            'question': self.ciq.get_current_question(),
            'answers': self.ciq.get_current_answers()
        }
        self.reset_all_buttons_color()
        self.socketio.emit('update_question_and_answers', current_QA)

    def button_clicked(self, button_value, current_user: User):
        if button_value == 'dance':
            self.dance()
            return
        # Check if the list is large enough to hold the value
        elif len(current_user.votes) <= self.ciq.question_index:
            # If not, append None values to the list until it is large enough
            current_user.votes.extend([None] * (self.ciq.question_index - len(current_user.votes) + 1))

        current_user.votes[self.ciq.question_index] = button_value
        print(current_user.votes)
        self.confirm_button_press(button_value, current_user)

    def dance(self):
        #dance
        pass

    def confirm_button_press(self, button_value, current_user):
        self.socketio.emit('confirm_button_press', button_value, room=current_user.id)

    def reset_all_buttons_color(self):
        self.socketio.emit('reset_buttons_color')
