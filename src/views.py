import base64
import json
import uuid
from io import BytesIO

import qrcode as qrcode
from flask import render_template, request, redirect, url_for, session, abort, Flask
from flask_socketio import join_room

from src.User import User
from src.SocketIOController import SocketIOController


def init_app(app: Flask, ciq, socketio):
    sIO = SocketIOController(ciq, socketio, session)

    @app.before_request
    def before_request():
        setup_new_user()

    @app.route('/', methods=['GET', 'POST'])
    def index():
        if request.method == 'POST':
            name = request.form['name']
            user = get_current_user()
            user.name = name
            update_total_names_display(ciq.users)
            return redirect(url_for('buttons'))
        return render_template('input.html', name=get_current_user_name())

    @app.route('/display-names')
    def display_names():
        url = request.url_root + "/"
        qr_code = generate_qr_code(url)
        names = get_all_names(ciq.users)

        return render_template('display-names.html', names=names, qr_code=qr_code)

    @app.route('/buttons', methods=['GET', 'POST'])
    def buttons():
        if request.method == 'POST':
            button_pressed = request.form['button']
            user = get_current_user()
            user.votes[ciq.question_index] = button_pressed

            return redirect('/buttons')

        return render_template('buttons.html', is_button_disabled=ciq.buttons_disabled)

    @app.route('/question', methods=['GET', 'POST'])
    def question():
        current_question = ciq.get_current_question()
        current_options = ciq.get_current_answers()
        return render_template('question.html', question=current_question, options=current_options)

    @app.route('/next_question')
    def next_question():
        ciq.advance_to_next_question()
        return "Next Question triggered"

    @app.route('/admin', methods=['GET', 'POST'])
    def admin():
        try:
            if get_user_by_id(session.get('user_id')).admin:
                if request.method == 'POST':  # If the form is submitted
                    checkbox_value = request.form.get('create_admin')
                    ciq.creating_new_admins_allowed = checkbox_value == 'on'
                    return '', 200
                else:
                    return render_template('admin.html',
                                           create_admin=ciq.creating_new_admins_allowed,
                                           users=ciq.users)
            else:
                abort(404)
        except KeyError:
            abort(404)

    @app.route('/new_admin')
    def new_admin():
        if ciq.creating_new_admins_allowed:
            user = get_current_user()
            user.admin = True
            return redirect(url_for('admin'))
        else:
            return "FORBIDDEN"

    @app.route("/main")
    def main():
        names = get_all_names(ciq.users)
        url = request.url_root + "/"
        qr_code = generate_qr_code(url)
        return render_template("main.html",
                               all_names=json.dumps(names),
                               qr_code=qr_code,
                               question=ciq.get_current_question(),
                               answers=ciq.get_current_answers())

    @app.route('/result')
    def result():
        return render_template('auswertung.html', result=ciq.get_result_dict())

    @app.route('/mobile_result')
    def mobile_result():
        return render_template('mobile_auswertung.html', data=ciq.get_current_user_result_dict(get_current_user()), name=get_current_user_name())

    @app.errorhandler(404)
    def page_not_found(e):
        easter = request.url_root + admin.__name__
        return render_template('404.html', home=url_for(index.__name__), egg=easter), 404

    @socketio.on('button_clicked')
    def button_clicked(data):
        sIO.button_clicked(data, current_user=get_current_user())

    @socketio.on('next_question')
    def request_next_question():
        sIO.request_next_question()

    @socketio.on('connect')
    def on_connect():
        join_room(get_current_user().id)

    def update_total_names_display(users: dict):
        # get all names from all users, only if they have a name
        names = get_all_names(users)
        socketio.emit('update_total_names_display', names)

    def confirm_button_press(button_value):
        socketio.emit('confirm_button_press', button_value)

    def get_all_names(users):
        names = [user.name for user in users if user.name is not None]
        print(names)
        return names

    def generate_qr_code(url):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=2,
        )
        qr.add_data(url)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")

        buffered = BytesIO()
        img.save(buffered)
        img_str = base64.b64encode(buffered.getvalue()).decode("utf-8")
        app.logger.info(f"Created New QR code: {url}")
        return img_str

    def get_current_user_name():
        user = get_user_by_id(session.get('user_id'))
        if user is not None:
            return user.name
        else:
            return None

    def get_user_by_id(user_id) -> User:
        for user in ciq.users:
            if user.id == user_id:
                return user
        return None

    def get_current_user() -> User:
        user = get_user_by_id(session['user_id'])
        if user is None:
            user = User(session['user_id'])
            ciq.users.append(user)
        return user

    def setup_new_user():
        if 'user_id' not in session:
            generated_id = uuid.uuid4()
            session['user_id'] = generated_id
        get_current_user()
