# Project Name: Creative Industry Quiz Game
For further information have a look at the [hackmd](https://hackmd.io/@k0da/Bkc3D0fGh)

## Description:
This project aims to create an engaging, multiplayer educational game focused on the creative industry and its various job categories. The game will be web-based and designed to be streamed to a screen while players interact using their phones. Up to 20 players will be able to participate in the game, moving around a virtual map to engage with the quiz component.

## Gameplay:
The gameplay involves players answering quiz questions using their phone's buttons. The quiz content will cover various aspects of the creative industry, and based on players' performance, they will be categorized at the end of the game. The categorization will help players identify their skills and knowledge related to the creative industry.

## Goals:

* Create an engaging and interactive educational game that appeals to a wide range of players.
* Educate players about the creative industry and help them identify their skills and potential job categories.
* Develop a web-based game that is easily accessible and playable on various devices, with the ability to stream to a TV and use phones as controllers.
* Encourage continuous learning by regularly updating the game with new quiz content and improvements based on user feedback.

## Setup:

1. install python(3.10 or newer) on your system (make sure you install pip)
2. run `pip install -r requirements.txt`
3. run `python app.py`
